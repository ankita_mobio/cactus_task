<html>
<head>
    <title>
        Add User Data
    </title>
</head>
<body>
<form action="{{url('/user/store')}}" name="add_form" id="app_form" style="border-radius: 0px;" method="post"
      class="form-horizontal group-border-dashed">

    <div class="form-group">
        <label class="col-sm-4 control-label">Name <span class="error"> :</span></label>
            <input type="text" name="name" id="name" placeholder="Name" class="form-control input-sm required"
                   value="{{old('name')}}"/>
    </div><br>

    <div class="form-group">
        <label class="col-sm-4 control-label">Country<span class="error"> :</span></label>
            <input type="text" name="country" id="country" placeholder="country" class="form-control input-sm required"
                   value="{{old('country')}}"/>
    </div><br>

    <div class="form-group">
        <label class="col-sm-4 control-label">Contact No<span class="error"> :</span></label>
            <input type="text" name="contact_no" id="contact_no" placeholder="contact_no" class="form-control input-sm required"
                   value="{{old('contact_no')}}"/>
    </div><br>

    {{ csrf_field() }}

    <div class="form-group">
        <div class="col-sm-6 col-md-8 savebtn">
            <p class="text-right">
                <button type="submit"
                        class="btn btn-space btn-info btn-lg">Add User</button>
            </p>
        </div>
    </div>

</form>

</body>
</html>