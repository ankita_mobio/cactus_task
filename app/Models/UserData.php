<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $table = 'user_datas';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'country', 'contact_n0', 'created_at', 'updated_at'
    ];

    /**
     * Add userdata
     *
     * @param array $models
     * @return boolean true | false
     */

    public function addUser(array $models = []){
        $user = new UserData();

        $user->name = $models['name'];
        $user->country = $models['country'];
        $user->contact_no = $models['contact_no'];

        $user_save = $user->save();

        if($user_save)
            return true;
        else
            return false;
    }
}
