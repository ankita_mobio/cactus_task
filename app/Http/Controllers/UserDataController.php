<?php

namespace App\Http\Controllers;

use App\Models\UserData;
use Illuminate\Http\Request;
use Validator;
use DB;

class UserDataController extends Controller
{
    protected $userData;

    public function __construct(UserData $userData)
    {
        $this->userData = $userData;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user_data.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //check validation for input field
        $validations = $this->customeValidate($request->all(), 'add');
        if ($validations) {
            return $validations;
        }

        try {
            $addUser = $this->userData->addUser($request->all());
            DB::commit();

        } catch (\Exception $e) {
            return response(['statusCode' => 0, 'cause' => '','errors' => $e->getMessage(), 'message' => '']);
        }

        if($addUser){
            return response(['statusCode' => 1, 'cause' => '','errors' =>'', 'message' => 'Record Insert.']);
        }else{
            return response(['statusCode' => 1, 'cause' => '','errors' =>'', 'message' => 'Something went to wrong.']);
        }
    }

    /**
     * Validation of add and edit action customeValidate
     *
     * @param array $data
     * @param string $mode
     * @return mixed
     */

    public function customeValidate($data, $mode)
    {
        $rules = array(
            'name' => 'required',
            'country' => 'required',
            'contact_no' => 'required|unique:user_datas|digits:10|numeric',
        );

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            return response(['statusCode' => 0, 'cause' => '','errors' => $validator->errors()->all(), 'message' => '']);
        }
        return false;
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function show(UserData $userData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function edit(UserData $userData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserData $userData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserData  $userData
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserData $userData)
    {
        //
    }
}
