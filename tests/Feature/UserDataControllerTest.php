<?php

namespace Tests\Feature;

use App\Http\Controllers\UserDataController;
use App\Models\UserData;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\App;
use Illuminate\View\View;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserDataControllerTest extends TestCase
{

    /**
     * @test
     */
    public function method_is_exist(){

        $this->assertTrue(method_exists(UserDataController::class, 'index'));
        $this->assertTrue(method_exists(UserDataController::class, 'store'));
        $this->assertTrue(method_exists(UserData::class, 'addUser'));
    }

    /**
     * @test
     */
    public function Check_Route_is_exit(){
        $this->get(route('user::adduser'))->assertStatus(200);
    }

    /**
     * @test
     */
    public function input_field_check(){

        $data = array(
            'name' => 'ankita',
            'contact_no' => '1234567890',
            'country' => 'india'
        );
        $this->assertTrue(isset($data['name']));
        $this->assertTrue(isset($data['contact_no']));
        $this->assertTrue(isset($data['country']));

    }

}
