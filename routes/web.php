<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(array('prefix' => 'user', 'as' => 'user::'), function() {
    Route::get('adduser',['as' => 'adduser', 'uses' => 'UserDataController@index']);
    Route::post('store',['as' => 'store', 'uses' => 'UserDataController@store']);
});